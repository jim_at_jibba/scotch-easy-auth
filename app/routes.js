module.exports = function(app, passport){

    // =====================================
    // HOME PAGE (with login links) ========
    // =====================================
    app.get('/', function(req, res){
        res.render('index.ejs'); // load index.ejs file
    });


    // =====================================
    // LOGIN ===============================
    // =====================================
    // show login form
    app.get('/login', function(req, res){

        // render the apge and pass in any flash data if it exists
        res.render('login.ejs', {message: req.flash('loginMessage') });
    });

    // process the login form
    // app.post('/login', all our passport stuff here);

    // =====================================
    // SIGN UP =============================
    // =====================================
    // we will want this protected so you have to be logged in to visit
    // we will use route middleware to verify this ( the isLoggedIn function)
    app.get('/profile', isLoggedIn, function(req, res){
        res.render('profile.ejs', {
            user: req.user // get the user out of session and pass to template
        });
    });

    // =====================================
    // LOGOUT ==============================
    // =====================================
    app.get('/logout', function(req, res){
        req.logout();
        res.redirect('/');
    });
};

// route middleware to make sure a user is logged in
function isLoggedIn(req, res, next){

    // if user is authenticated in the session, carry on
    if(req.isAuthenticated())
        return next();

    // if they are not redirect them to the home page
    res.redirect('/');
}




